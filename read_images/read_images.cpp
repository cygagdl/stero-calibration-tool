#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <stdio.h>
#include <iostream>

using namespace std;
using namespace cv;

int x = 0;
void DrawConors(Mat &frame);

struct chkBoard
{
    int boardHeight;
    int boardWidth;
    float cellSize;
    cv::Size cbSize;

    chkBoard(int height, int width, float cellsize)
    {
        boardHeight = height;
        boardWidth = width;
        cellSize = cellsize;
        cbSize = cv::Size(width,height);
    }
};

int main(int argc, char const *argv[])
{
  char* imgs_directory = "../pair_images";
  char* extension = "jpg";

  VideoCapture cap1("rtspsrc latency=300 location=rtsp://admin:a1234567@192.168.4.101:554 ! rtph264depay ! h264parse ! vaapidecode ! videoconvert ! video/x-raw,format=(string)BGR ! appsink",cv::CAP_GSTREAMER);
  VideoCapture cap2("rtspsrc latency=300 location=rtsp://admin:a1234567@192.168.4.102:554 ! rtph264depay ! h264parse ! vaapidecode ! videoconvert ! video/x-raw,format=(string)BGR ! appsink",cv::CAP_GSTREAMER);
  Mat img1, img2, img1_show, img2_show;
  while (1) {
    cap1 >> img1;
    cap2 >> img2;
    if(!img1.data || !img2.data)
    {
        continue;
    }

    img1_show = img1.clone();
    img2_show = img2.clone();

    DrawConors(img1_show);
    DrawConors(img2_show);

    cv::resize(img1_show,img1_show,cv::Size(960,540));
    cv::resize(img2_show,img2_show,cv::Size(960,540));
    imshow("IMG1", img1_show);
    imshow("IMG2", img2_show);

    if (waitKey(30) == 's') {
      x++;
      char filename1[200], filename2[200];
      sprintf(filename1, "%s/left%d.%s", imgs_directory, x, extension);
      sprintf(filename2, "%s/right%d.%s", imgs_directory, x, extension);

      cout << "filename1: " << filename1 << endl;

      cout << "Saving img pair " << x << endl;
      imwrite(filename1, img1);
      imwrite(filename2, img2);
    }
  }
  return 0;
}


void DrawConors(cv::Mat& frame)
{
    cv::Mat mGray;
    chkBoard pBoard(5,6,0.07);

    cv::cvtColor(frame,mGray,CV_BGR2GRAY);
    //detect chessboard corners
    std::vector<cv::Point2f> pv2DCorners;
    bool found = cv::findChessboardCorners(mGray, pBoard.cbSize, pv2DCorners, cv::CALIB_CB_FAST_CHECK);
    if (found){
        cv::drawChessboardCorners(frame, pBoard.cbSize, cv::Mat(pv2DCorners), found);
    }
}
