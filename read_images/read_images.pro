TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = ../../apps

SOURCES += \
    read_images.cpp

HEADERS +=

INCLUDEPATH += /home/dev/Documents/Modules/opencv-3.4.1-tbb/install/include \
                /home/dev/Documents/Modules/opencv-3.4.1-tbb/install/include/opencv \
                /home/dev/Documents/Modules/opencv-3.4.1-tbb/install/include/opencv2
INCLUDEPATH += /home/dev/Documents/Modules/opencv-3.4.1-tbb/build/include
LIBS += -L"/home/dev/Documents/Modules/opencv-3.4.1-tbb/build/lib" -lopencv_world

LIBS += -lpopt
