#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <iostream>
#include <mutex>
#include <thread>
#include <atomic>
#include <list>

using namespace std;
using namespace cv;

vector< vector< Point3f > > object_points;
vector< vector< Point2f > > image_points;
vector< Point2f > corners;
vector< vector< Point2f > > left_img_points;

std::list< cv::Mat> vFrames;
std::recursive_mutex FramesMutex;

std::atomic<bool> bufferRun{true};

struct chkBoard
{
    int boardHeight;
    int boardWidth;
    float cellSize;
    cv::Size cbSize;

    chkBoard(int height, int width, float cellsize)
    {
        boardHeight = height;
        boardWidth = width;
        cellSize = cellsize;
        cbSize = cv::Size(width,height);
    }
};

void StreamThread(cv::VideoCapture* cap)
{
    cv::Mat image;
    while (bufferRun){
        (*cap) >> image;
        if(image.data)
        {
            std::lock_guard<std::recursive_mutex> plk(FramesMutex);

            while(vFrames.size() > 3){
                vFrames.pop_front();
            }
            vFrames.push_back(image.clone());
        }
        else
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(20));
        }
    }
}

cv::Mat getFrame()
{
    cv::Mat ret;
    {
        std::lock_guard<std::recursive_mutex> plk(FramesMutex);
        if(!vFrames.empty())
        {
            ret = vFrames.front();
            vFrames.pop_front();
        }
    }
    return ret;
}

void setup_calibration(int board_width, int board_height,
                       float square_size, std::vector< cv::Mat >& calib_images) {
    Size board_size = Size(board_width, board_height);
    Mat img, gray;
    for (int k = 0; k < calib_images.size(); k++) {
        img = calib_images[k];
        cv::cvtColor(img, gray, CV_BGR2GRAY);

        bool found = false;
        found = cv::findChessboardCorners(img, board_size, corners,
                                          CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
        if (found)
        {
            cornerSubPix(gray, corners, cv::Size(5, 5), cv::Size(-1, -1),
                         TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
            drawChessboardCorners(gray, board_size, corners, found);
        }

        vector< Point3f > obj;
        for (int i = 0; i < board_height; i++)
            for (int j = 0; j < board_width; j++)
                obj.push_back(Point3f((float)j * square_size, (float)i * square_size, 0));

        if (found) {
            cout << k << ". Found corners!" << endl;
            image_points.push_back(corners);
            object_points.push_back(obj);
        }
    }
}

double computeReprojectionErrors(const vector< vector< Point3f > >& objectPoints,
                                 const vector< vector< Point2f > >& imagePoints,
                                 const vector< Mat >& rvecs, const vector< Mat >& tvecs,
                                 const Mat& cameraMatrix , const Mat& distCoeffs) {
    vector< Point2f > imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    vector< float > perViewErrors;
    perViewErrors.resize(objectPoints.size());

    for (i = 0; i < (int)objectPoints.size(); ++i) {
        projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,
                      distCoeffs, imagePoints2);
        err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);
        int n = (int)objectPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err*err/n);
        totalErr += err*err;
        totalPoints += n;
    }
    return std::sqrt(totalErr/totalPoints);
}

int main(int argc, char const **argv)
{
    std::string rtsp_uri = "rtsp://admin:a1234567@192.168.4.102:554";

    std::string type = "right";

    const char* out_file = "../data/cam_right.yml";

    int frameW, frameH;
    std::vector< cv::Mat > calib_images;

    std::string m_rtsp_uri = std::string("rtspsrc ")
            + "latency=300 "
            + "location=" + rtsp_uri + " "
            + "! rtph264depay ! h264parse "
            + "! vaapidecode "
            + "! videoconvert ! video/x-raw,format=(string)BGR ! appsink";

    cv::VideoCapture m_cap;
    m_cap.open(m_rtsp_uri,cv::CAP_GSTREAMER);
    if(!m_cap.isOpened())
    {
        std::cout << "fail to open video!" << std::endl;
    }
    frameH= m_cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    frameW  = m_cap.get(CV_CAP_PROP_FRAME_WIDTH);
    cv::Size frameSize(frameW,frameH);

    std::thread frameBuf(StreamThread,&m_cap);

    chkBoard pBoard(5,6,0.07);

    cv::Mat frame;
    cv::Mat mGray;
    cv::Mat visFrame;
    uchar sign;
    int order = 0;
    while(1)
    {
        frame=getFrame();
        if(!frame.data)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(20));
            continue;
        }

        visFrame = frame.clone();
        cv::cvtColor(frame,mGray,CV_BGR2GRAY);
        std::vector<cv::Point2f> v2DCorners;
        //输入图像，角点数目，存储角点的变量
        bool found = cv::findChessboardCorners(mGray, pBoard.cbSize, v2DCorners, cv::CALIB_CB_FAST_CHECK);

        if (found){
            cv::drawChessboardCorners(visFrame, pBoard.cbSize, cv::Mat(v2DCorners), found);
        }
        cv::resize(visFrame,visFrame,cv::Size(960,540));
        cv::imshow("vis",visFrame);
        sign = cv::waitKey(15);
        if(sign == 's')
        {
            calib_images.push_back(frame.clone());
            std::cout<<order++<<" image saved!"<<std::endl;
        }
        else if(sign == 'q')
        {
            bufferRun=false;
            if(frameBuf.joinable())
            {
                frameBuf.join();
            }
            break;
        }
    }
    m_cap.release();

    setup_calibration(pBoard.boardWidth, pBoard.boardHeight, pBoard.cellSize, calib_images);

    printf("Starting Calibration\n");
    Mat K;
    Mat D;
    vector< Mat > rvecs, tvecs;
    int flag = 0;
    flag |= CV_CALIB_FIX_K4;
    flag |= CV_CALIB_FIX_K5;
    calibrateCamera(object_points, image_points, frameSize, K, D, rvecs, tvecs, flag);

    cout << "Calibration error: " << computeReprojectionErrors(object_points, image_points, rvecs, tvecs, K, D) << endl;

    FileStorage fs(out_file, FileStorage::WRITE);
    fs << "K" << K;
    fs << "D" << D;
    fs << "board_width" << pBoard.boardWidth;
    fs << "board_height" << pBoard.boardHeight;
    fs << "square_size" << pBoard.cellSize;
    fs << "ip" << rtsp_uri;
    fs << "type" << type;
    printf("Done Calibration\n");

    return 0;
}
